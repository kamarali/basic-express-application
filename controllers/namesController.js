const express = require('express')
const router = express.Router()
const namesMiddleware = require('./../service/namesService')

router.get('/:name', namesMiddleware.sayHello)

module.exports = router
