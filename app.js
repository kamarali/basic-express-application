var express = require('express')
var app = express()

// Using the controllers directory to define routes
app.use(require('./controllers'))

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
