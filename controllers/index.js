const express = require('express')
const router = express.Router()

// Any request which has /names in the path will be handled in the namesController
router.use('/names', require('./namesController'))

// A request sent to the / route will return "Hello World"
router.get('/', function (req, res) {
  res.send('Hello World!')
})

module.exports = router
